<?php
	// import head section
	include_once 'partials/header.php';
?>

<div class="container">
	<h1>Тук можете да регистрирате/видите преподавателите.</h1>
	<div class="registration">
		<div class="form-container">
			<form class="registrationform" action="register.php" method="post">
				<input type="text" name="name" placeholder="Име на преподавател">
				<input type="text" name="speciality" placeholder="Специалност">
				<input type="text" name="workPlace" placeholder="Работно място">
				<input type="submit" value="Добави">
			</form>
		</div> 	<!-- form-container ends -->
	</div>	<!-- registration ends -->
	<div class="show-content">
		<table>
			<tr>
				<th>
					Име
				</th>
				<th>
					Специалност
				</th>
				<th>
					Работно място
				</th>
			</tr>
			<tbody>
				<tr>
					<td>1</td>
					<td>2</td>
					<td>3</td>
				</tr>
			</tbody>
		</table>
	</div>	<!-- show-content ends -->
</div> <!-- container ends -->


<?php
	// import footer.
	include_once 'partials/footer.php';

?>
