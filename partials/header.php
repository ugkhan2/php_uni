<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
</head>
<body>
	<header>
		<nav>
			<ul>
				<li><a href="./">Home</a></li>
				<li><a href="lecturers.php">Lecturer</a></li>
				<li><a href="lectures.php">Lectures</a></li>
				<li><a href="organizations.php">Organizations</a></li>
				<li><a href="guests.php">Guests</a></li>
			</ul>
		</nav>
	</header>
